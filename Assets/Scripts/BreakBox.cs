﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakBox : MonoBehaviour {


    //definiowanie obiektów , animatorow, innych zmiennych (bool)
    private GameObject player;
    private Animator anim;
    public Animator playerAnimation;
    public GameObject otherObject;
    private bool isColliding;
    public GameObject explosionPrefab;
    public PlayerInteraction playerInteractionScript;



    private void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        isColliding = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Awake()
    {
        playerAnimation = otherObject.GetComponent<Animator>();
    }


    // gdy wejdziemy w kolizję z objektem boxa uruchamiaja się poszczegolne funkcje
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            isColliding = true;

            if(isColliding)
            {
                anim.GetComponent<Animator>().SetBool("isStepped", true);
                //playerAnimation.SetBool("jumpBOX", false);
                playerAnimation.SetBool("jumpBOX", true);
                playerAnimation.SetBool("jump", false);
                player.GetComponent<PlayerInteraction>().jumperonbox();
                Destroy(gameObject, 1f);
            }
        }
    }
    // gdy wyjdziemy z kolizji to wszystkie poszczegolne funkcje wracaja do bool=false
    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isColliding = false;

            if (!isColliding)
            {
                anim.GetComponent<Animator>().SetBool("isStepped", false);
                playerAnimation.SetBool("jumpBOX", false);
            }
        }
    }

    // wywolanie eksplozji skrzynii oraz jego usuniecie po 2 sekundach :)

    public void explosion()
    {
       GameObject clone = Instantiate(explosionPrefab, transform.position, transform.rotation);
       Destroy(clone, 2f);
    }

  
}
