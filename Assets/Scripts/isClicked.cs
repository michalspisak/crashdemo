﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class isClicked : MonoBehaviour {

    // definiowanie obiektów
    public GameObject canvas;
    public GameObject canvasWin;
    public GameObject Box1;
    public GameObject Box2;
    public GameObject Box3;
    public GameObject Box4;
    public GameObject Box5;


    //metoda która uruchamia się tylko raz
    void Start()
    {
        //ustawianie canvasu startowego
        canvas.SetActive(true);
        //ustawianie canvasu koncowego
        canvasWin.SetActive(false);
    }

    void Update()
    {
        //jezeli wszystkie skrzynie zostana zniszczone to uruchamia 
        //sie canvas koncowy oraz korutyna ktora po 4 sekundach uruchamia scene na nowo

        if (Box1==null && Box2 == null && Box3==null && Box4==null && Box5 == null)
        {
            canvasWin.SetActive(true);

            //korutyna ktora wczytuje nowa scene oraz odblokowuje kursor
            StartCoroutine(Wait());
        }
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(4f);
        Application.LoadLevel(0);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
}
