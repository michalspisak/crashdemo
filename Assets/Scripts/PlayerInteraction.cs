﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour {


    //definiowanie zmiennych poruszania sie
    [System.Serializable]
    public class MoveSettings
    {
        public float forwardVel = 12;
        public float rotateVel = 100;
        public float jumpVel = 25;
        public float distToGrounded = 0.1f;
        public LayerMask ground;
    }

    [System.Serializable]
    public class PhysSettings
    {
        public float downAccel = 0.75f;
    }
    [System.Serializable]
    public class InputSettings
    {
        public float inputDelay = 0.1f;
        public string FORWARD_AXIS = "Vertical";
        public string TURN_AXIS = "Horizontal";
        public string JUMP_AXIS = "Jump";
    }
    
    public Animator anim;
    


    public MoveSettings moveSetting = new MoveSettings();
    public PhysSettings physSettings = new PhysSettings();
    public InputSettings inputSettings = new InputSettings();


    Vector3 velocity = Vector3.zero;
    Quaternion targetRotation;
    Rigidbody rBody;
    float forwardInput, turnInput, jumpInput;

    public Quaternion TargetRotation
    {
        get { return targetRotation; }
    }

    bool Grounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, moveSetting.distToGrounded, moveSetting.ground);
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        anim = GetComponent<Animator>();
        targetRotation = transform.rotation;
        if (GetComponent<Rigidbody>())
        {
            rBody = GetComponent<Rigidbody>();
        }
        else
        {
            Debug.LogError("Character needs a rigidbody");

        }

        forwardInput = turnInput = jumpInput = 0;
    }

    void GetInput()
    {
        forwardInput = Input.GetAxis(inputSettings.FORWARD_AXIS);
        turnInput = Input.GetAxis(inputSettings.TURN_AXIS);
        jumpInput = Input.GetAxisRaw(inputSettings.JUMP_AXIS);
    }

    void Update()
    {
        GetInput();
        Turn();
    }

    private void FixedUpdate()
    {
        Run();
        Jump();

        rBody.velocity = transform.TransformDirection(velocity);
       
    }

    public void Jump()
    {
        if(jumpInput>0 && Grounded())
        {
            //skakanie
            velocity.y = moveSetting.jumpVel;
            anim.SetBool("jump",true);
            StartCoroutine(waiting());
        }
        else if ( jumpInput == 0 && Grounded())
        {
            //zero
            velocity.y = 0;
        }
        else
        {
            velocity.y -= physSettings.downAccel*1.1f;
            
        }
    }

 

    public void jumperonbox()
    {
        velocity.y = moveSetting.jumpVel*1.5f;
        velocity.y -= physSettings.downAccel * 10;
        anim.SetBool("jump", true);
        StartCoroutine(waiting());
    }

    private IEnumerator waiting()
    {
        yield return new WaitForSeconds(1f);
        anim.SetBool("jump", false);
    }

    void Run()
    {
        if(Mathf.Abs(forwardInput)>inputSettings.inputDelay)
        {
            //poruszanie sie
            velocity.z = moveSetting.forwardVel*forwardInput;
            anim.SetBool("run", true);
        }
        else
        {
            //zero
            velocity.z = 0;
            anim.SetBool("run", false);
        }
    }
    void Turn()
    {
        if (Mathf.Abs(turnInput) > inputSettings.inputDelay)
        {
            targetRotation *= Quaternion.AngleAxis(moveSetting.rotateVel * turnInput * Time.deltaTime, Vector3.up);
        }
        transform.rotation = targetRotation;
    }
}

