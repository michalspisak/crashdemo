﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    public GameObject box;
    public float power = 2.0f;
    public float radius = 0.1f;
    public float upForce = 1.0f;
    public GameObject explosionPrefab;


	// Use this for initialization
	void Start () {
		
	}

    private void FixedUpdate()
    {
        if(box==enabled)
        {
            Invoke("Detonate", 1);
        }
    }

    void Detonate()

    {
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        Vector3 explosionPosition = box.transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if(rb!=null)
            {
                rb.AddExplosionForce(power, explosionPosition, radius, upForce, ForceMode.Impulse);
            }
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
